#!/bin/bash

vendor/bin/php-cs-fixer fix --dry-run

errors=$?

 if [ $errors -gt 0 ]; then
    vendor/bin/php-cs-fixer fix
    git add --all
    git commit -m "PHP CS FIXER FIX"
    git push
    exit 0;
 fi

exit 0;
