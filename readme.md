# Square1 Laravel Code-Style preset

Include our default code-style rules for `eslint` and `php-cs-fixer`.

## Install

Add the following repository to your `composer.json` file

```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:square1/code-style-preset.git"
        }
    ]
}
```

Register the service provider on your `config/app.php` file

```php
//
"providers" => [
    Square1\LaravelCodeStylePreset\CodeStyleServiceProvider::class,
]
//
```

Then install via composer

```bash
composer require square1/code-style-preset
```

After install, publish the presets:

```bash
php artisan vendor:publish --provider="Square1\LaravelCodeStylePreset\CodeStyleServiceProvider"
```

## Run php cs fixer

`php-cs-fixer` can fix the style issues auto-magically. Run this to fix the issues:

```
vendor/bin/php-cs-fixer fix
``` 

## Configure pipelines

Add this to check code style issues in every commit so no code with a style issue gets merged.

```
vendor/bin/php-cs-fixer fix --dry-run
```

## Autofix using bitbucket pipelines

To run the code fixer automatically using Bitbucket Pipelines, just add a new step to your `default` block as follows:

```
steps:
  - step: &phpCodeFix
      name: php-cs-fix
      caches:
        - composer
      script:
        - composer install
        - sh vendor/square1/code-style-preset/php-cs-fix.sh
```

The full `bitbucket-pipelines.yml` will look something like this:

```yaml
image:
  name: dregistry.sq1.io/php8.0-fpm-compv2:stable
definitions:
  services:
    redis:
      image: redis
steps:
  - step: &phpCodeFix
      name: php-cs-fix
      caches:
        - composer
      script:
        # Check code style and bail if fails
        - composer install
        - sh vendor/square1/code-style-preset/php-cs-fix.sh
pipelines:
  default:
    - step: *phpCodeFix
```