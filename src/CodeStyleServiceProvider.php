<?php

namespace Square1\LaravelCodeStylePreset;

use Illuminate\Support\ServiceProvider;

class CodeStyleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([__DIR__.'/../.php-cs-fixer.dist.php' => base_path('.php-cs-fixer.dist.php')]);
        $this->publishes([__DIR__.'/../.eslintrc' => base_path('.eslintrc')]);
    }
}
